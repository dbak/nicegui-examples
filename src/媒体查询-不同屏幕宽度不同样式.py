from nicegui import ui

ui.add_head_html(
    r"""
<style>



@media only screen and (max-width: 1440px) {
    .自定义类{
        background:green;
    }

    body{
        --gap:50px;
    }
}   


@media only screen and (max-width: 850px) {
    .自定义类{
        background:yellow;
    }

    body{
        --gap:20px;
    }
}   

@media only screen and (max-width: 680px) {
    body{
        --gap:2px;
    }

    .自定义类{
         background:red;
    }
}   

</style>

"""
)


ui.label("随屏幕大小，颜色变化").classes("自定义类")

with ui.row().classes("w-full").style("gap:var(--gap)"):
    ui.label("xxxx").classes("bg-blue grow")
    ui.label("yyyy").classes("bg-red grow")


ui.separator()
ui.label("下面试试 tw css 的")
ui.label("随屏幕大小，颜色变化").classes("min-[680px]:bg-red max-[1000px]:bg-blue")
