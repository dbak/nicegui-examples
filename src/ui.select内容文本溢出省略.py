"""
效果：当人为限制 ui.select 宽度时，如果文本过长超出范围，希望有限度显示文本内容，并带省略号收尾
本代码只针对单行文本
"""

from nicegui import ui

# ellipsis-select 为自定义名字，主要作用是能对特定的 ui.select 生效
# 具体 css 编写，只需要搜索 "css 文本溢出 省略"

ui.add_head_html(
    r"""<style>
    /*这个是选项菜单的*/
    .ellipsis-select .q-item__label{
     overflow: hidden; 
     white-space: nowrap; 
     text-overflow: ellipsis; 
    }

    /*这个设置input框的*/
    .ellipsis-select .q-field__native > span{
     overflow: hidden; 
     white-space: nowrap; 
     text-overflow: ellipsis; 
    }
</style>"""
)

ui.select.default_props("outlined")

# select的输入框，需要限定 select 的宽度，并且设置自定义的类名(ellipsis-select)
# select的选项菜单，需要通过 props 设置 popup-content-class ，限定菜单宽度和自定义的类名
ui.select(["xxxxxxxxxxxxxxxxxxxx" * 5, "a"]).classes("w-[20ch] ellipsis-select").props(
    'popup-content-class="w-[20ch] ellipsis-select" '
)

# 这个选项菜单没有省略效果，但是输入框有
ui.select(["xxxxxxxxxxxxxxxxxxxx" * 5, "a"]).classes("w-[30ch] ellipsis-select")


ui.run()
