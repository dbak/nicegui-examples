from nicegui import ui
from luckysheet import Luckysheet


@ui.page("/sheet")
def page_sheet():
    def on_cellMousedown(e):
        ui.notify(f"on_cellMousedown:{e}")

    def on_cellUpdateBefore(e):
        ui.notify(f"on_cellUpdateBefore:{e}")

    async def onclick():
        # 可以调用的api https://mengshukeji.gitee.io/LuckysheetDocs/zh/guide/api.html#api
        value = await sheet.run_api("getCellValue", 0, 0)
        ui.notify(value)

    # ui
    ui.button("get", on_click=onclick)

    # 可配置的 hook https://mengshukeji.gitee.io/LuckysheetDocs/zh/guide/config.html#%E9%92%A9%E5%AD%90%E5%87%BD%E6%95%B0
    sheet = Luckysheet(
        {"title": "testing", "lang": "zh"},
        hook={
            "cellMousedown:args => ([args[1]])": on_cellMousedown,
            "cellUpdateBefore": on_cellUpdateBefore,
        },
    ).classes("w-full h-[100vh]")


ui.link("sheet", page_sheet)


ui.run()
