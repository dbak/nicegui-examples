import { convertDynamicProperties } from "../../static/utils/dynamic_properties.js";

export default {
    template: '<div></div>',
    mounted() {

        const luckysheet = window.luckysheet
        delete this.options['container']
        // debugger
        const hooks = Object.entries(this.hookMapping).map(([key, callbackId]) => {
            const splitPos = key.indexOf(':')

            if (splitPos < 0) {
                return [
                    key, (...args) => {
                        this.$emit("onHook", { hookId: callbackId, args })
                    }
                ]
            }

            const newKey = key.slice(0, splitPos)
            const argsFn = new Function("return " + key.slice(splitPos + 1))()
            return [
                newKey, (...args) => {
                    this.$emit("onHook", { hookId: callbackId, args: argsFn(args) })
                }
            ]

        })

        const hookSetting = Object.fromEntries(hooks)

        const createOpts = {
            container: this.contarnerId,
            hook: hookSetting,
            ...this.options
        }
        luckysheet.create(Object.freeze(createOpts))
    },
    methods: {
        runApi(name, ...args) {
            if (name.startsWith(":")) {
                name = name.slice(1);
                args = args.map((arg) => new Function("return " + arg)());
            }
            return window.luckysheet[name](...args);
        }
    },
    props: {
        options: Object,
        contarnerId: String,
        hookMapping: Object,
    },
};
