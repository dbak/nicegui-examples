from nicegui import ui, app
import orjson as json
from pathlib import Path


# 也可以执行下载数据，通过定义 get请求传送数据
def load_map_data():
    file = Path(__file__).parent / "geo_data.json"
    return json.loads(file.read_bytes())


#
@app.get("/api/map")
def get_map_data():
    return load_map_data()


# 里面用的是另一个站点的web api，你可以把地址换成自己的"/api/map"
# https://datav.aliyun.com/portal/school/atlas/area_selector
ui.add_body_html(
    r"""
<script>

window.addEventListener('DOMContentLoaded', () => {

    fetch("https://geo.datav.aliyun.com/areas_v3/bound/100000_full.json")
    .then((response) => response.json())
    .then((data) => {
        echarts.registerMap('map1',data)
    });  

});
</script>
"""
)


# 目前的点击事件处理机制有问题，无法兼容地图，会报错
def on_chart_click(e):
    print(e)


echart = ui.echart(
    {
        "series": [
            {},
        ],
    },
    on_chart_click,
).classes("w-full h-[50vh]")


def onclick():
    echart.options["series"][0] = {
        "name": "test",
        "type": "map",
        "map": "map1",
        "roam": True,
    }
    echart.update()


# 暂时没想到方法自行加载，或者可以先弄一个一次性的定时任务搞着。
ui.button("load map data", on_click=onclick)


ui.run()
