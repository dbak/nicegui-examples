import pandas as pd
import io
from nicegui import ui


def onclick():
    data = io.BytesIO()
    pd.DataFrame({"name": list("abcd")}).to_excel(data)
    data.seek(0)
    ui.download(data.read(), "test.xlsx")


ui.button("dw", on_click=onclick)

ui.run()
