

## Why?

ex4ng 中的数据响应式以函数为主，灵活性较高，但是缺乏规范。这里尝试找到一种以类的组织方式，并且尽可能避免暴露响应式语义。

- `utils.py` 为通用代码
- `test_`前缀的文件为使用示例

## 概念
参考 [reflex](https://reflex.dev/docs/state/vars/) 的方式。

### 自定义状态类

```python
import utils as rx

class NumberState(rx.RefState):
    number = 0

    @rx.var
    def value_text(self):
        return f"随机数字为：{self.number}"

    def update(self):
        # 修改了 number ，将会自动触发 value_text ，并更新界面
        self.number = random.randint(0, 100)


# 实例化状态
state = NumberState()

# 直接传入 var 属性即可
rx.RxLabel(state.value_text)


ui.button("update", on_click=state.update)

```

- 继承 `RefState`
- 所有定义的类变量(前缀没有下划线)，会自动转换为响应式变量
- `@rx.var` 装饰的对象方法，自动转换成响应式计算数据