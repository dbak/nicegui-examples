from nicegui import ui
import utils as rx


class NumberState(rx.RefState):
    text = "hello"

    @rx.var
    def upper(self):
        return self.text.upper()

    @rx.var
    def label_color(self):
        if len(self.text) > 8:
            return "red"

        return "green"

    @rx.var
    def input_outlined(self):
        if self.label_color.value == "red":
            return True

        return False


state = NumberState()


rx.RxInput(value=state.text).bind_props("outlined", state.input_outlined)
rx.RxLabel(state.upper).bind_style("color", state.label_color)

ui.run()
