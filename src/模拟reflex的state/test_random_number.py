from nicegui import ui
import utils as rx
import random


class NumberState(rx.RefState):
    number = 0

    def update(self):
        self.number = random.randint(0, 100)

    def is_even(self):
        return self.number % 2 == 0

    @rx.var
    def value_text(self):
        return f"随机数字为：{self.number}"

    @rx.var
    def even_text(self):
        return ["偶数", "奇数"][self.is_even()]

    @rx.var
    def text_color(self):
        return ["green", "red"][self.is_even()]


state = NumberState()


rx.RxLabel(state.value_text)
rx.RxLabel(state.even_text).bind_style("color", state.text_color)


ui.button("update", on_click=state.update)


ui.run()
