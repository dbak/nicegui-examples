from nicegui import ui

# 抄 quasar 文档示例中的 style 页签中的代码
ui.add_sass(
    r"""
.my-sticky-header-table
  /* height or max-height is important */
  height: 310px

  .q-table__top,
  .q-table__bottom,
  thead tr:first-child th
    /* bg color is important for th; just specify one */
    background-color: #c1f4cd

  thead tr th
    position: sticky
    z-index: 1
  thead tr:first-child th
    top: 0

  /* this is when the loading indicator appears */
  &.q-table--loading thead tr:last-child th
    /* height of all previous header rows */
    top: 48px
"""
)

columns = [
    {
        "name": "name",
        "label": "Name",
        "field": "name",
        "required": True,
        "align": "left",
    },
    {"name": "age", "label": "Age", "field": "age", "sortable": True},
]
rows = [{"name": f"A{i}", "age": i} for i in range(100)]
ui.table(columns=columns, rows=rows, row_key="name").classes("my-sticky-header-table")


# 锁列

# 抄 quasar 文档示例中的 style 页签中的代码
ui.add_sass(
    r"""
.my-sticky-column-table
  /* specifying max-width so the example can
    highlight the sticky column on any browser window */
  max-width: 600px

  thead tr:first-child th:first-child
    /* bg color is important for th; just specify one */
    background-color: #fff

  td:first-child
    background-color: #f5f5dc

  th:first-child,
  td:first-child
    position: sticky
    left: 0
    z-index: 1
"""
)


columns = [
    {
        "name": f"col{i}",
        "label": f"col{i}",
        "field": f"col{i}",
    }
    for i in range(10)
]
rows = [{f"col{i}": f"data-{i}" for i in range(10)} for i in range(5)]

ui.table(columns=columns, rows=rows, row_key="name").classes("my-sticky-column-table")


ui.run()
