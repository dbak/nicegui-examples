from nicegui import ui

img_src = "https://picsum.photos/200/300"

with ui.card().classes("w-full h-[50vh]"):
    img = ui.image(img_src).props("fit=contain")


def on_change():
    img._style["transform"] = f"scale({knob.value})"
    img.update()


knob = ui.knob(1, show_value=True, on_change=on_change)


ui.run()
