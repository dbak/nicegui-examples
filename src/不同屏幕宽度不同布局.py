from nicegui import ui
from ex4nicegui.layout import grid_box, mark_area

ui.label.default_classes("outline")


# ex4nicegui 根据屏幕宽度动态布局
def ex4ng_layout():
    # 大屏幕下布局
    area1 = """
    ........ title    ........
    content1 content2 content3
    """

    # 小屏幕，全部竖排
    area2 = """
    title
    content1
    content2
    content3
    """

    with grid_box(area1, break_point=">sm[1024px-Infinity]").grid_box(
        area2, break_point="<md[0-1023.99px]"
    ):
        #

        ui.label("这是标题") + mark_area("title")

        ui.label("这是content1") + mark_area("content1")
        ui.label("这是content2") + mark_area("content2")
        ui.label("这是content3") + mark_area("content3")


# 利用 tw css 根据屏幕宽度动态布局
# https://tailwindcss.com/docs/responsive-design
def ng_layout():
    ui.label("这是标题").classes("self-center")

    with ui.element("div").classes("w-full flex flex-col lg:flex-row"):
        ui.label("这是content1").classes("grow")
        ui.label("这是content2").classes("grow")
        ui.label("这是content3").classes("grow")


with ui.tabs() as tabs:
    ui.tab("ex4ng")
    ui.tab("ng 原始做法")

with ui.tab_panels(tabs, value="ex4ng").classes("w-full"):
    with ui.tab_panel("ex4ng"):
        ex4ng_layout()

    with ui.tab_panel("ng 原始做法"):
        ng_layout()


ui.run()
